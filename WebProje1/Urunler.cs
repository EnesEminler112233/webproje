//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebProje1
{
    using System;
    using System.Collections.Generic;
    
    public partial class Urunler
    {
        public int UrunID { get; set; }
        public Nullable<int> KategoriID { get; set; }
        public string Tur { get; set; }
        public string Adi { get; set; }
        public string Resim { get; set; }
        public Nullable<double> Fiyat { get; set; }
        public string Beden { get; set; }
        public string Model { get; set; }
        public string Renk { get; set; }
        public string Marka { get; set; }
        public string Açıklama { get; set; }
        public Nullable<int> SatanID { get; set; }
        public Nullable<int> UcuzID { get; set; }
        public Nullable<int> YeniID { get; set; }
        public string Slider { get; set; }
    }
}
