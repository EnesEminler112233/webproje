﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProje1.Models
{
    public class KategoriDto
    {
        public int KatID { get; set; }
        public string KatName { get; set; }
    }
}