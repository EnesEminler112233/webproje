﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje1;
using WebProje1.Models;
using WebProje1.Models.Data;

namespace Web1.Controllers
{
    public class HomeController : Controller
    {
        KismetKonfeksiyonEntities db = new KismetKonfeksiyonEntities();
        // GET: Home
        public ActionResult Index()
        {
            List<KategoriDto> katListe = new List<KategoriDto>();
            katListe = db.Kategori.Select(s => new KategoriDto
            {
                KatID = s.KatID,
                KatName = s.KategoriAd,
            }).ToList();

            Session["Kategoriler"] = katListe;

            var model = db.Urunler.ToList();
            return View(model);
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult Company()
        {

            return View();
        }
        public ActionResult Products(int? KatID)
        {
            List<Urunler> list = new List<Urunler>();

            if (KatID != null)
            {
                list = db.Urunler.Where(x => x.KategoriID == KatID).ToList();
            }
            else
            {
                list = db.Urunler.ToList();
            }

            return View(list);
        }
        //   [ChildActionOnly]

        public ActionResult UrunDetay(int? ID)
        {
            var model = db.Urunler.Where(x => x.UrunID == ID);
            return View(model);
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

    }

}