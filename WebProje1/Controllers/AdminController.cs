﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje1;
using WebProje1.Models.Data;

namespace WebProje1.Controllers
{
    public class AdminController : Controller
    {
        KismetKonfeksiyonEntities db = new KismetKonfeksiyonEntities();
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Satan()
        {
            var model = db.Urunler.ToList();
            return View(model);
        }
        public ActionResult SatanEkle(int? ID)
        {
            List<Urunler> list = new List<Urunler>();

            if (ID != null)
            {
                list = db.Urunler.Where(x => x.UrunID == ID).ToList();
            }
            else
            {
                list = db.Urunler.ToList();
            }
         
            return View(list);
        }
        public ActionResult Ucuz()
        {
            var model = db.Urunler.ToList();
            return View(model);
        }
        public ActionResult UcuzEkle(int? ID)
        {
            List<Urunler> list = new List<Urunler>();

            if (ID != null)
            {
                list = db.Urunler.Where(x => x.UrunID == ID).ToList();
            }
            else
            {
                list = db.Urunler.ToList();
            }

            return View(list);
        }
        public ActionResult Yeni()
        {
            var model = db.Urunler.ToList();
            return View(model);
        }
        public ActionResult YeniEkle(int? ID)
        {
            List<Urunler> list = new List<Urunler>();

            if (ID != null)
            {
                list = db.Urunler.Where(x => x.UrunID == ID).ToList();
            }
            else
            {
                list = db.Urunler.ToList();
            }

            return View(list);
        }
    }

}